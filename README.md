MetaSS README
================
# Installation

To install the package please use the following command.
``` r
devtools::install_gitlab("KarikSiemund/MetaSS")
```

# Introduction

In this package you can use the following six metaheuristic algorithms
to perform specification search.

-   Tabu Search
-   Simulated Annealing
-   Genetic Algorithm
-   Adaptive Genetic Algorithm
-   Particle-Swarm-Optimization
-   Hybrid Ant-Colony-Optimization Algorithm

# Procedure

Given your fitted lavaan model, you create a parameter table using the
function

``` r
search.prep()
```

which will be the base for our specification search. It contains a
binary indicator vector, where each entry represents a possibly to be
estimated parameter. After defining a strategy (what types pf parameters
should be explored), the parameter table is handed to one of the
algorithms with the original fitted lavaan model. The algorithms will
always output the best model they can suggest in addition to algorithm
specific information about the search.

# Example

``` r
# Load libraries also for test data
library(MetaSS)
library(lavaan)

# Get test data and specify test model
data("HolzingerSwineford1939")

HS.model <- ' visual  =~ x1 + x2 + x3
              textual =~ x4 + x5 + x6
              speed   =~ x7 + x8 + x9'

# Fit model and efine exploratory strategy
fit <- lavaan(HS.model, data=HolzingerSwineford1939,
              auto.var=TRUE, auto.fix.first=TRUE,
              auto.cov.lv.x=TRUE)

ptab <- search.prep(fit)

# Test algorithms
tabu.sem(fit, ptab)
simulated_annealing.sem(fit, ptab)
genetic.sem(fit, ptab)
adaptive_genetic.sem(fit, ptab)
pso.sem(fit, ptab)
haco.sem.parallel(fit, ptab)
```

Additional information on the package can be found under https://gitlab.lrz.de/KarikSiemund/specification-search-as-cop-simulation.git.
