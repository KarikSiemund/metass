#' Adaptive Genetic Algorithm
#'
#' Performs specification search on a fitted lavaan model using an adaptive Genetic Algorithm.
#'
#' @param init.model Fitted lavaan model to be advanced with the algorithm
#' @param ptab Parameter Table from the function search.prep
#' @param obj Fit indices used in objective function; currently not configurable
#' @param n.pop Population size
#' @param n.parents Number of parents
#' @param n.mut Number of mutations
#' @param lambda Penalty parameter
#' @param n.gen Number of generations that don't improve solutions; termination criterion
#' @param k1 Hyperparameter controlling adaptive probabilities
#' @param k2 Hyperparameter controlling adaptive probabilities
#' @param k3 Hyperparameter controlling adaptive probabilities
#' @param k4 Hyperparameter controlling adaptive probabilities
#' @return A list containing the best model and population information
#' @export
#' @import lavaan
#' @examples
#' adaptive_genetic.sem()

adaptive_genetic.sem<-function(init.model=fit.start,
                      ptab=spec.table,
                      obj=fitmeasures(init.model)[c("srmr", "cfi", "rmsea")],
                      n.pop = 30,
                      n.parents = 8,
                      n.mut = 6,
                      lambda = .5,
                      n.gen=100,
                      k1 = 1,
                      k2 = 0.05,
                      k3 = 1,
                      k4 = 0.1){

  # Initialize objective function, and parameters and save initial model
  obj.func <- function(lambda, obj, free, num.params){
    ((1-lambda)*
       ((((1/(1+exp(-55*(0.08-obj[1])))))+(1-(1/(1+exp(-55*(0.95-obj[2])))))+
           ((1/(1+exp(-55*(0.06-obj[3]))))))/3))+
      (lambda*
         (1-(sum(free)/num.params)))
  }

  ptab$free <- free <- ptab$free
  ptab.child <- start.ptab <- ptab.mut <- ptab
  num.params <- length(free)
  model.new <- model.child <- refit.model(init.model, ptab)
  obj.current <- obj.new <- obj.func(lambda, fitmeasures(model.new)[c("srmr", "cfi", "rmsea")], free, num.params)

  #generate starting population (init.model + random individuals)
  population.model <- population.free <- vector("list", length = n.pop)
  population.fit <- rep(NA, n.pop)
  population.model[[1]] <- model.new
  population.free[[1]] <- free
  population.fit[1] <- obj.func(lambda, fitmeasures(model.new)[c("srmr", "cfi", "rmsea")], free, num.params)

  #do as long as we have 10 valid individuals (no error)
  i <- 2
  while (any(is.na(population.fit))) {
    n.free <- sample(5:num.params-5, 1) #sample number of loadings to free (5 arbitrary)
    which.free <- sample(1:num.params, n.free)
    start.free <- rep(0, num.params)
    start.free[which.free] <- 1
    start.ptab$free <- start.free
    population.free[[i]] <- start.free
    
    

      if(ident_check(start.free, start.ptab)){
      population.model[[i]] <- refit.model(init.model, start.ptab)

      #only take acceptable models

      skip_to_next <- FALSE

      tryCatch(fitmeasures(population.model[[i]], c("srmr", "cfi", "rmsea")), error = function(e) { skip_to_next <<- TRUE})

      if(!skip_to_next) {
        population.fit[i] <- obj.func(lambda, c(fitmeasures(population.model[[i]], fit.measures = "srmr"),
                                                fitmeasures(population.model[[i]], fit.measures = "cfi"),
                                                fitmeasures(population.model[[i]], fit.measures = "rmsea"))
                                      , start.free, num.params)

        i=i+1}}else{} #skip updating index if error
  }

  print("Starting Population found!")

  #save fit to compare if it changed as stopping criterion, initialize counter
  population.fit.old <- population.fit
  k <- 1

  #algorithm (also skip if lavaan cannot get solution for fit)
  while (k <= n.gen) {

    #Crossover/ Reproduction
    #select parents, split point
    #and generate offspring
    parents <- prob_cross <- vector("list", n.parents)
    childs <- list()
    obj.children <- vector()

    #DIFF Selection based on Roulette Wheel Selection
    for (j in 1:n.parents) {

      #DIFF samples split.point randomly in each iteration and for each parent
      parents[[j]] <- sample(sort(population.fit, index.return = T)$ix,
                             2,
                             prob = sort(population.fit/sum(population.fit)))}

    #DIFF calculate (adaptive) probability for crossover
    prob_cross <- vector()
    max_fit <- max(population.fit)
    mean_fit <- mean(population.fit)

    for (n in 1:n.parents) {
      if(max(population.fit[parents[[n]]]) >= mean_fit){
        prob_cross[n] = k1*(max_fit-max(population.fit[parents[[n]]]))/(max_fit-mean_fit)
      }else{prob_cross[n] = k3}
    }

    # DIFF probability of crossover applied here
    for (j in 1:n.parents) {
      if(runif(1) < prob_cross[j]){
        split.point <- sample(2:(num.params-1), 1)
        #split.point <- round(num.params/2)
        childs <- append(childs,
                         list(c(population.free[[parents[[j]][1]]][1:split.point],
                                population.free[[parents[[j]][2]]][(split.point+1):num.params]))
        )
      }
    }

    #calculate fitness of children
    if (length(childs) > 0){

      for (j in 1:length(childs)){
        
        ptab.child.ident <- ptab.child
        ptab.child.ident$free <- childs[[j]]
        
        if(ident_check(childs[[j]], ptab.child.ident))

          ptab.child$free <- childs[[j]]

          model.child <- refit.model(init.model, ptab.child)

          skip_to_next <- FALSE

          tryCatch(fitmeasures(model.child, c("srmr", "cfi", "rmsea")), error = function(e) { skip_to_next <<- TRUE})

          if(!skip_to_next) {obj.new <-  fitmeasures(model.child, c("srmr", "cfi", "rmsea"))}else{obj.new <- c(1,0,1)} #always worse

        obj.new <- obj.func(lambda, obj.new, childs[[j]], num.params)

        obj.children <- append(obj.children, obj.new)
      }


      # Mutation
      # calculate adaptive Mutation probabilities
      prob_mut <- vector()
      for (m in 1:length(childs)){
        if (obj.children[m] >= mean_fit){
          prob_mut <- append(prob_mut,
                             (k2*(max_fit - obj.children[m]))/
                               (max_fit-mean_fit))
        }else{prob_mut <- append(prob_mut, k4)}
      }

      # add lower bound
      prob_mut[prob_mut < 0.005] <- 0.005
      # mutate children
      for (j in 1:length(childs)) {
        childs[[j]] = unlist(lapply(childs[[j]], FUN = function(x) abs(ifelse(runif(1) < prob_mut[j], x-1, x))))
      }

      #evaluate children and add them to population when better
      for (j in 1:length(childs)) {

        #check if offspring is already in population, if no continue
        if(!any(population.free %in% list(childs[[j]]))){
          
          ptab.child.ident <- ptab.child
          ptab.child.ident$free <- childs[[j]]
          
          if(ident_check(childs[[j]], ptab.child.ident)){

            ptab.child$free <- childs[[j]]

            #fit child and try extract fit
            model.child <- refit.model(init.model, ptab.child)

            skip_to_next <- FALSE

            tryCatch(fitmeasures(model.child, c("srmr", "cfi", "rmsea")), error = function(e) { skip_to_next <<- TRUE})

            if(!skip_to_next) {obj.new <-  fitmeasures(model.child, c("srmr", "cfi", "rmsea"))}else{obj.new <- c(1,0,1)} #always worse

            obj.new <- obj.func(lambda, obj.new, childs[[j]], num.params)

            #check if fit is better (larger) then any in the population
            #if yes - update
            if(min(population.fit) < obj.new){
              if(length(which(population.fit == min(population.fit))) == 1){
                unfittest.ind <- which(population.fit == min(population.fit))}else{
                  unfittest.ind <-sample(which(population.fit == min(population.fit)), 1)
                }
              population.fit[unfittest.ind] <- obj.new
              population.free[[unfittest.ind]] <- ptab.child$free
              population.model[[unfittest.ind]] <- model.child
            }
        }
      }
      }
    }
    #update counter if population didnt change (fitness)
    if(all(population.fit.old == population.fit)){k <- k+1}else{k <- 1}

    population.fit.old <- population.fit

  }


  list(model_final = population.model[[min(which.max(population.fit))]],
       free_final = population.free[[min(which.max(population.fit))]],
       models = population.model,
       fitness = population.fit)


}
