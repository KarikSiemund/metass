#' Hybrid Ant-Colony-Optimization Algorithm
#'
#' Performs specification search on a fitted lavaan model using a hybrid Ant-Colony-Optimization Algorithm.
#'
#' @param init.model Fitted lavaan model to be advanced with the algorithm
#' @param ptab Parameter Table from the function search.prep
#' @param obj Fit indices used in objective function; currently not configurable
#' @param tabu.size Size of Tabu List
#' @param ban.cutoff.srmr Cutoff to put a model on the banned list
#' @param ban.cutoff.cfi Cutoff to put a model on the banned list
#' @param ban.cutoff.rmsea Cutoff to put a model on the banned list
#' @param delta.entropy Small enough change in entropy to stop algorithm
#' @param penalty.par Penalty parameter
#' @return A list containing the best model and pheromone
#' @export
#' @import lavaan
#' @import doParallel
#' @examples
#' haco.sem.parallel()

haco.sem.parallel<-function(init.model=fitted.model,
                            ptab=ptab,
                   obj=fitmeasures(init.model)[c("srmr", "cfi", "rmsea")],
                   tabu.size = 3,
                   ban.cutoff.srmr = 0.2,
                   ban.cutoff.cfi = .75,
                   ban.cutoff.rmsea = 0.2,
                   penalty.par = 0.1,
                   delta.entropy = 0.001)
{

  # initialize: spec. vector, initial pheromone, tabu and ban list, core number
  obj.func <- function(obj){
    ((1-((exp(-55*(0.08-obj[1]))/(1+exp(-55*(0.08-obj[1]))))))+
       ((exp(-55*(0.95-obj[2]))/(1+exp(-55*(0.95-obj[2]))))) +
       (1-((exp(-55*(0.06-obj[3]))/(1+exp(-55*(0.06-obj[3])))))))/3
  }
  free.current <- free.cand <- ptab$free
  n.params <- length(free.current)
  model <- refit.model(init.model, ptab)
  ban <- candidates.free <- tabu <- list()
  candidates.pher <- vector()
  acc.pher <- rep(1/n.params, n.params)
  obj.save <- fitmeasures(refit.model(init.model, ptab))[c("srmr", "cfi", "rmsea")]
  entropy.old <- -sum((acc.pher/sum(acc.pher))*log2((acc.pher/sum(acc.pher))))
  entropy.curr <- entropy.diff <- 1
  nc <- 1

  #find initial model with non-zero pher if init.model is too bad
  obj <- obj.func(fitmeasures(refit.model(init.model, ptab), fit.measures = c("srmr", "cfi", "rmsea")))
  penalty <- penalty.par * log2(1+(sum(free.cand)/n.params))
  pher.cand <- max(c(0, obj - penalty))

  if(pher.cand == 0){

    while (pher.cand == 0 || rmsea > ban.cutoff.rmsea || srmr > ban.cutoff.srmr || cfi < ban.cutoff.cfi || !model@Fit@converged) {

      #random sample model
      free.cand <- sample(c(0,1), n.params, replace = T)

      if(!any(free.cand %in% list(ban))){
        
        ptab.ident <- ptab
        ptab.ident$free <- free.cand

        #check identification
        if(ident_check(free.cand, ptab)){

          ptab$free <- free.cand
          model <- refit.model(init.model, ptab)

          #extract rmsea, keep one for ban and standardize other for pheromone
          skip_to_next <- FALSE

          tryCatch(fitmeasures(model, c("srmr", "cfi", "rmsea")), error = function(e) { skip_to_next <<- TRUE})

          if(!skip_to_next) {
            obj <- fitmeasures(model, c("srmr", "cfi", "rmsea"))
            srmr <- obj[1]
            cfi <- obj[2]
            rmsea <- obj[3]
          }else{
            obj <- c(1,0,1)
            srmr <- 1
            cfi <- 0
            rmsea <- 1
          }

          obj <- obj.func(obj)
          penalty <- penalty.par * log2(1+(sum(free.cand)/n.params))
          pher.cand <- max(c(0, obj - penalty))

        }

        #update ban
        if(rmsea > ban.cutoff.rmsea || srmr > ban.cutoff.srmr || pher.cand == 0 || cfi < ban.cutoff.cfi || !model@Fit@converged || !ident_check(free.cand, ptab)){
          ban <- append(ban, list(free.cand))
        }else{acc.pher <- acc.pher + (pher.cand*free.cand)}
      }
    }

  }

  #reset obj and set current model
  obj <- obj.save
  free.current <- free.start <- free.cand
  print("Starting model found!")

  succ.iter <- 1

  #algorithm
  while (entropy.diff > delta.entropy) {

    #set cores
    registerDoParallel(cores = nc)

    #go through all neighbours (skip members of tabu and ban)
    candidates <- foreach(j = 1:n.params, .combine = "rbind",
                          .packages = "lavaan", .maxcombine = n.params+1)%do%{

                            free.cand <- free.current

                            free.cand[j] <- ifelse(free.current[j] == 1, 0, 1)

                            if(!any(free.cand %in% list(ban)) && !any(free.cand %in% list(tabu))){
                              
                              ptab.ident <- ptab
                              ptab.ident$free <- free.cand
                              
                              if(!ident_check(free.cand)){
                                pher.cand = 0
                                srmr <- 1
                                cfi <- 0
                                rmsea <- 1}else{


                                  ptab$free <- free.cand
                                  model <- refit.model(init.model, ptab)

                                  #extract rmsea, keep one for ban and standardize other for pheromone
                                  skip_to_next <- FALSE

                                  tryCatch(fitmeasures(model, c("srmr", "cfi", "rmsea")), error = function(e) { skip_to_next <<- TRUE})

                                  if(!skip_to_next) {
                                    obj <- fitmeasures(model, c("srmr", "cfi", "rmsea"))
                                    srmr <- obj[1]
                                    cfi <- obj[2]
                                    rmsea <- obj[3]
                                  }else{
                                    obj <- c(1,0,1)
                                    srmr <- 1
                                    cfi <- 0
                                    rmsea <- 1
                                  }

                                  obj <- obj.func(obj)
                                  penalty <- penalty.par * log2(1+(sum(free.cand)/n.params))
                                  pher.cand <- max(c(0, obj - penalty))
                                }


                              #update ban
                              if(rmsea > ban.cutoff.rmsea || srmr > ban.cutoff.srmr || pher.cand == 0 || cfi < ban.cutoff.cfi || !model@Fit@converged){
                                ban <- append(ban, list(free.cand))
                                c(rep(1, n.params), 0)
                              }else{
                                #save non banned models
                                c(free.cand, pher.cand)
                              }
                            }
                          }

    #update tabu
    tabu <- append(tabu, list(free.current), 0)
    tabu <- tabu[-(tabu.size+1)]

    #update acc.pher
    for(i in 1:n.params){
      if(candidates[i,n.params+1] != 0){
        acc.pher <- acc.pher + (candidates[i,n.params+1]*candidates[i,1:n.params])}
    }

    #extract model with best pher (if more then one -> random sample)
    free.current <- candidates[which(candidates[,n.params+1] == max(candidates[,n.params+1])), 1:n.params]
    free.cand <- free.current
    pher.current <- max(candidates[,n.params+1])

    #reset candidates and calculate entropy
    entropy.curr <- -sum((acc.pher/sum(acc.pher))*log2((acc.pher/sum(acc.pher))))
    entropy.diff <- abs(entropy.old - entropy.curr)
    entropy.old <- entropy.curr
    candidates <- 0
    succ.iter <- succ.iter + 1
  }


  #find best model
  max.pher <- max(acc.pher)

  q <- seq(.8, .98, by = 0.01)

  free.final <- matrix(nrow = length(q), ncol = n.params)

  for (i in 1:length(q)) {
    free.final[i,which(acc.pher >= (q[i]*max.pher))] <- 1
    free.final[i,-which(acc.pher >= (q[i]*max.pher))] <- 0
  }

  #set cores
  registerDoParallel(cores = 1)

  pher.final <- foreach(i=1:length(q), .combine = c, .packages = "lavaan")%do%{
    
    ptab.ident <- ptab
    ptab.ident$free <- as.vector(free.final[i,])
    
    if(!ident_check(as.vector(free.final[i,]), ptab)){
      obj <- c(1,0,1)
      srmr <- 1
      cfi <- 0
      rmsea <- 1
    }else{
      
      
      ptab$free <- as.vector(free.final[i,])

      model <- refit.model(init.model, ptab)

      skip_to_next <- FALSE

      tryCatch(fitmeasures(model, c("srmr", "cfi", "rmsea")), error = function(e) { skip_to_next <<- TRUE})

      if(!skip_to_next) {
        obj <- fitmeasures(model, c("srmr", "cfi", "rmsea"))
        srmr <- obj[1]
        cfi <- obj[2]
        rmsea <- obj[3]
      }
    }
    
    obj <- obj.func(obj)
    penalty <- penalty.par * log2(1+(sum(free.final[i,])/n.params))
    max(c(0, obj - penalty))
  }

  #sometimes this throws an error (return least parsimonious model if so)
  if(min(which(pher.final == max(pher.final))) != Inf){
    ptab$free <- free.final[min(which(pher.final == max(pher.final))),]
  }else{
    ptab$free <- free.final[1,]
  }
  model <- refit.model(init.model, ptab)


  list(free_final = ptab$free,
       model_final = model,
       pheromone = acc.pher)
}
